import java.util.List;

public class MyObjectList {
    private List<MyObject> myObjectList;

	public List<MyObject> getMyObjectList() {
		return myObjectList;
	}

	public void setMyObjectList(List<MyObject> myObjectList) {
		this.myObjectList = myObjectList;
	}
    
    
}