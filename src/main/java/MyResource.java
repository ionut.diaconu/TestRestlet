import java.util.ArrayList;
import java.util.List;

import org.restlet.ext.jackson.JacksonRepresentation;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

public class MyResource extends ServerResource {

	@Get("json")
	public Representation getMyObject() {
		List<MyObject> objs = new ArrayList<>();

		MyObject a1 = new MyObject();
		a1.setAge(12);
		a1.setName("a1");
		objs.add(a1);

		MyObject a2 = new MyObject();
		a2.setAge(13);
		a2.setName("a2");
		objs.add(a2);

		MyObjectList objList = new MyObjectList();
		objList.setMyObjectList(objs);

		return new JacksonRepresentation<MyObjectList>(objList);

	}

}
