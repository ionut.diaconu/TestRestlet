import org.restlet.Application;
import org.restlet.Component;
import org.restlet.Restlet;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class TestRestletApplication extends Application {

	//http://localhost:8182/test/myres
	public static void main(String[] args) throws Exception {
		Component component = new Component();
		component.getServers().add(Protocol.HTTP, 8182);

		Application app = new TestRestletApplication();

		component.getDefaultHost().attach("/test", app);
		component.start();
	}

	@Override
	public Restlet createInboundRoot() {

		Router router = new Router(getContext());

		router.attach("/myres", MyResource.class);
		return router;

	}

}